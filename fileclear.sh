#!/bin/bash

#
# 配置方式：
#   1:上传配置文件以及执行文件至目录/etc/fileclear
#   2:依照当前系统环境修改fileclear.conf
#   3:设置可执行权限  chmod +x /etc/fileclear/fileclear.sh
#   4:配置定时任务,查看命令：crontab -l, 命令：crontab -e 进入编辑状态添加(每天凌晨1点自动执行): 0 1 * * * bash /etc/fileclear/fileclear.sh
#

###参数设置

# 定义清理模式(1:保留最小X数量文件/2:保留最少x天文件/3:保留最小X数量文件或者保留最少x天文件,二者条件符合一个即保留), 默认为 3
clearmode=3
# 定义保留的最小数量，默认为 30
reservednum=30
# 定义保留的最小天数，默认为 30
reservedday=30
# 处理任务最大线程数
THREADCOUNT=5
# 配置要处理的文件信息配置
FILECONF=/etc/fileclear/fileclear.conf
# 是否保存清理信息日志(在清理目录下留存)
KEEPCLEARLOG=true

###默认值处理
if [[ -z $clearmode || $clearmode -eq 0 ]]; then
    clearmode=3
fi
if [[ -z $reservednum || $reservednum -eq 0 ]]; then
    reservednum=30
fi
if [[ -z $reservedday || $reservedday -eq 0 ]]; then
    reservedday=30
fi


# 获取已定义的要清理文件信息
fileClearArr=()
fileCount=0
while read line
do
    fileline=$line
    if [[ -z $fileline || $fileline =~ ^# ]] ;then
        continue
    fi
    singleFileStr=${fileline//|/ }
    singleFileCount=0
    for singleFile in $singleFileStr   
    do  
        ((singleFileCount++))
    done
	canfileclear=false
	if [ "$singleFileCount" -eq 2 ]; then
        canfileclear=true
        singleFileStr="$singleFileStr $clearmode $reservednum $reservedday"
    elif [ "$singleFileCount" -eq 3 ]; then
        canfileclear=true
        singleFileStr="$singleFileStr $reservednum $reservedday"
    elif [ "$singleFileCount" -eq 5 ]; then
        canfileclear=true
    fi
	if [ "$canfileclear" = false ]; then
		continue
	fi
    fileClearArr[$fileCount]=$singleFileStr
    ((fileCount++))
done < $FILECONF

# 如果无清理文件信息则退出
if [ "$fileCount" -eq 0 ]; then
   exit 1
fi

# 获取线程数
if [[ -z $THREADCOUNT || $THREADCOUNT -eq 0 ]]; then
    THREADCOUNT=1
fi
if [ $fileCount -lt $THREADCOUNT ]; then
    THREADCOUNT=$fileCount
fi

#创建线程管道
[ -e /tmp/fileclear.fifo ] || mkfifo /tmp/fileclear.fifo
exec 2<>/tmp/fileclear.fifo
rm -rf /tmp/fileclear.fifo
for ((i = 1 ; i <= $THREADCOUNT ; i++ ))
do
    echo >&2
done

for ((i = 0 ; i < $fileCount ; i++))
do
read -u2
{
    singleFileStr=${fileClearArr[i]}
    singleFileCount=0
    for singleFile in $singleFileStr   
    do  
        singleFileArr[$singleFileCount]=$singleFile
        ((singleFileCount++))
    done
	sub_basdir=${singleFileArr[0]}
    sub_fileregex=${singleFileArr[1]}
    sub_clearmode=${singleFileArr[2]}
    sub_reservednum=${singleFileArr[3]}
	sub_reservedday=${singleFileArr[4]}
    mkdir -p $sub_basdir
	if [ "$KEEPCLEARLOG" = true ]; then
		touch $sub_basdir/fileclear.log
	fi
    unset singleFileArr
	echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') -- 正在查找($sub_basdir)下符合条件($sub_fileregex)的文件执行清理， 清理规则为：clearmode=$clearmode / reservednum=$sub_reservednum / reservedday=$sub_reservedday ..."
	clearfilecount=0
	# 假如保留最少文件数模式
	if [[ $sub_clearmode -eq 1 || $sub_clearmode -eq 3 ]]; then
		filenum=$(echo $sub_fileregex | xargs -I {} find $sub_basdir -name {} | wc -l)
		while (($filenum > $sub_reservednum))
		do
			if [ $sub_clearmode -eq 3 ] ;then
				delefile=$(echo $sub_fileregex | xargs -I {} find $sub_basdir -name {} -mtime +$sub_reservedday -print0 | xargs -0 -r ls -t | tail -1)
			else
				delefile=$(echo $sub_fileregex | xargs -I {} find $sub_basdir -name {} -print0 | xargs -0 -r ls -t | tail -1)
			fi
			if [ -z $delefile ] ;then
				unset delefile
				break
			fi
			rm -rf "$delefile"
			if [ "$KEEPCLEARLOG" = true ]; then
				echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') -- 删除文件: $delefile " | tee -a $sub_basdir/fileclear.log
			fi
			((clearfilecount++))
			((filenum--))
			unset delefile
		done		
	elif [ $sub_clearmode -eq 2 ]; then
        while ((1 > 0))
		do
			delefile=$(echo $sub_fileregex | xargs -I {} find $sub_basdir -name {} -mtime +$sub_reservedday -print0 | xargs -0 -r ls -t | tail -1)
			if [ -z $delefile ] ;then
				unset delefile
				break
			fi
			rm -rf "$delefile"
			if [ "$KEEPCLEARLOG" = true ]; then
				echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') -- 删除文件: $delefile " | tee -a $sub_basdir/fileclear.log
			fi
			((clearfilecount++))
			unset delefile
		done		
	fi
	echo "-- $(date +'%Y年%m月%d日 %H时%M分%S秒') -- ($sub_basdir)下符合条件($sub_fileregex)的文件执行清理结束，共清理$clearfilecount个文件， 清理规则为：clearmode=$clearmode / reservednum=$sub_reservednum / reservedday=$sub_reservedday ..."
    #将令牌放回管道
    echo >&2
}&
done
wait
exec 2<&-                       
exec 2>&-

